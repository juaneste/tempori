package com.pdg.tempori.database.tasks

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "tasks",
    foreignKeys = [ForeignKey(
        entity = TypeTask::class,
        parentColumns = ["task_class", "type"],
        childColumns = ["task_class", "type"]
    )])
data class Task(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0L,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "description")
    val description: String,
    @ColumnInfo(name = "task_class")
    val taskClass: Long,
    @ColumnInfo(name = "type")
    val type: Int,
    @ColumnInfo(name = "date")
    val date: Date,
    @ColumnInfo(name = "status")
    val status: Int, // enum
    @ColumnInfo(name = "reminder")
    val reminder: String // enum
)

@Entity(
    tableName = "task_class"
)
data class TaskClass(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0L,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "active")
    val active: Boolean = true
)

@Entity(
    tableName = "type_task",
    primaryKeys = ["task_class", "type"],
    foreignKeys = [ForeignKey(
        entity = TaskClass::class,
        parentColumns = ["id"],
        childColumns = ["task_class"]
    )]
)
data class TypeTask(
    @ColumnInfo(name = "task_class")
    val taskClass: Long,
    @ColumnInfo(name = "type")
    val type: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "img")
    val img: String, // enum
    @ColumnInfo(name = "is_external")
    val isExternal: Boolean = false
)
/*
enum class ReminderType(val time: Long,key: String){

}

 */

/*
enum class TaskImage(val img: Int, val key: String){
    ICON(R.drawable.splash_icon, "SplashIcon")
}
//println(TaskImage.valueOf("ICON").rgb)
//println(TaskImage.ICON.rgb)
//println(TaskImage.values().toList())

 */