package com.pdg.tempori.database.tasks

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface TaskDatabaseDao {

    @Insert
    fun inset(task: Task)

    @Update
    fun update(task: Task)

    @Query("Select * from tasks where id = :key")
    fun get(key: Long): Task

    @Query("Delete from tasks")
    fun clear()

    @Query("Select * from tasks order by tasks.date asc")
    fun getAllNights(): LiveData<List<Task>>

    @Query("SELECT * from tasks WHERE id = :key")
    fun getTaskWithId(key: Long): LiveData<Task>
}