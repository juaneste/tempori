package com.pdg.tempori.components.calendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.pdg.tempori.R
import com.pdg.tempori.components.calendar.CalendarWeekAdapter.WeekViewholder
import com.pdg.tempori.database.tasks.Task
import java.text.SimpleDateFormat
import java.util.*

class CalendarWeekAdapter : RecyclerView.Adapter<WeekViewholder>(){

    var data = listOf<Map<Int, List<Task>>>()

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: WeekViewholder, position: Int) {
        val item = data[position]

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekViewholder {
        return WeekViewholder.from(parent)
    }


    class WeekViewholder private constructor(view : View ) :
        RecyclerView.ViewHolder(view){

        fun bind(tasks: Map<Int, List<Task>>){

        }

        companion object {
            fun from(parent: ViewGroup): WeekViewholder {
                val layoutInflater = LayoutInflater.from(parent.context)

                val view = layoutInflater.inflate(R.layout.fragment_profile, parent, false) // borrar

                // val binding = ListItemSleepNightBinding.inflate(layoutInflater, parent, false)

                return WeekViewholder(view)
            }
        }
    }

    data class DayInfo private constructor(
        val date: String,
        val weekDay: Int,
        val weekKey: String
    ){
        var tasks :List<Task>? = null
        companion object{
            val format = SimpleDateFormat("yyyy-mm-dd")
            val calendar = Calendar.getInstance()
            fun getDayInfo(date: Date): DayInfo{
                calendar.setTime(date)
                val weekKey = "${calendar.get(Calendar.YEAR)}_${calendar.get(Calendar.WEEK_OF_YEAR)}"
                return  DayInfo(format.format(date), calendar.get(Calendar.DAY_OF_WEEK), weekKey)
            }
        }
    }





}