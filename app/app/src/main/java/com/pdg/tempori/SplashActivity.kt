package com.pdg.tempori

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pdg.tempori.core.TemporiActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //application.setTheme(R.style.AppTheme);

        val next = getnextIntent()
        startActivity(next)
        finish()
    }

    private fun getnextIntent(): Intent {
        return Intent(this@SplashActivity, TemporiActivity::class.java)
    }
}
