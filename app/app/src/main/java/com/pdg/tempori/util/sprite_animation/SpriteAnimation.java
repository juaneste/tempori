package com.pdg.tempori.util.sprite_animation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class SpriteAnimation extends SpriteObject{
    public final static int FPS_VALOR = 5;

    private Rect sRectangle;
    private int fps;
    private int numFrames;
    private int currentFrame;
    private long frameTimer;

    private int spriteHeight;
    private int spriteWidth;

    private boolean loop;
    public boolean dispose;

    public SpriteAnimation(int w, int h, SpritesInfo.ISpriteEnum tipo) {
        super(0, 0, tipo);

        numFrames = tipo.getFrames();
        // inicializar();
        inicializar(imagen, w, h, numFrames, true);

    }


    public void inicializar() {
        sRectangle = new Rect(0, 0, imagen.getWidth(), imagen.getHeight());
        frameTimer = 0;
        currentFrame = 0;
        dispose = false;
        fps = 1000 / FPS_VALOR;
    }

    public void inicializar(Bitmap bitmap, int w, int h, int frameCount,
                            boolean loop) {
        this.numFrames = frameCount;
        this.imagen = bitmap;
        this.tamY = imagen.getHeight();
        this.tamX = imagen.getWidth() / numFrames;
        this.sRectangle = new Rect(0, 0, imagen.getWidth(), imagen.getHeight());
        this.sRectangle.top = 0;
        this.sRectangle.bottom = (int) tamY;
        this.sRectangle.left = 0;
        this.sRectangle.right = (int) tamX;
        this.fps = 1000 / FPS_VALOR;
        this.loop = loop;
        spriteHeight = h;
        spriteWidth = w;
    }


    @Override
    public void pintar(Canvas c, int xCentro, int yCentro, float escala) {
        // TODO Auto-generated method stub
        int cx = (int) (xCentro + posX);
        int cy = (int) (yCentro + posY);
        int tx = (int) (escala * spriteWidth / 2);
        int ty = (int) (escala * spriteHeight / 2);
        Rect dst = new Rect(cx - tx, cy - ty, cx + tx, cy + ty);
        c.drawBitmap(imagen, sRectangle, dst, null);
    }

    public void pintarFijo(Canvas c, int xCentro, int yCentro, float escala) {
        // TODO Auto-generated method stub
        int tx = (int) (escala * spriteWidth / 2);
        int ty = (int) (escala * spriteHeight / 2);
        Rect dst = new Rect(xCentro - tx, yCentro - ty, xCentro + tx, yCentro
                + ty);
        c.drawBitmap(imagen, sRectangle, dst, null);
    }

    public void Update(long gameTime) {
        if (gameTime > frameTimer + fps) {
            frameTimer = gameTime;
            currentFrame += 1;

            if (currentFrame >= numFrames) {
                currentFrame = 0;

                if (!loop)
                    dispose = true;
            }

            sRectangle.left = (int) (currentFrame * tamX);
            sRectangle.right = (int) (sRectangle.left + tamX);
        }
    }

}
