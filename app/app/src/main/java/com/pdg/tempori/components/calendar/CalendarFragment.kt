package com.pdg.tempori.components.calendar



import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.pdg.tempori.R
import com.pdg.tempori.databinding.FragmentCalendarBinding
import java.text.DateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CalendarFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding : FragmentCalendarBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_calendar, container,false
        )
        val cal = java.util.Calendar.getInstance()
        DateFormat.getInstance().format(cal)


        return binding.root
    }


    private fun calculateWeeks(){

            Calendar.getInstance().let {calendar ->
                //Inicia con la fecha actual
                val previousWeeks = 2;
                val nextWeeks = 10
                val beginMonday = true
                val firstDayWeek = if(beginMonday) Calendar.MONDAY else Calendar.SUNDAY
                // encuentra el primer día de la semana
                calendar.add(Calendar.DATE, firstDayWeek - calendar.get(Calendar.DAY_OF_WEEK))

                // imprime la primera fecha
                println(DateFormat.getInstance().format(calendar.getTime()))
                // retrocede para ver las dos semanas anteriores
                calendar.add(Calendar.WEEK_OF_YEAR, -previousWeeks)
                // imprime la fecha del rango
                print("Desde: ")
                println(DateFormat.getInstance().format(calendar.getTime()))

                calendar.add(Calendar.WEEK_OF_YEAR, previousWeeks + nextWeeks)
                // imprime la fecha del rango
                print("Hasta: ")
                println(DateFormat.getInstance().format(calendar.getTime()))
            }



    }

}
