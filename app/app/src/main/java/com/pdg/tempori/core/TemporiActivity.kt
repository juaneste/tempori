package com.pdg.tempori.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pdg.tempori.R

class TemporiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tempori)
    }
}
