package com.pdg.tempori.components.calendar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import com.pdg.tempori.R
import com.pdg.tempori.util.sprite_animation.SpriteAnimation
import com.pdg.tempori.util.sprite_animation.SpritesInfo
import kotlin.math.min

class WeekDayView  @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var dayName: String = ""
        set(value: String){
            field = value
            invalidate()
        }

    private var dayNumber: Int = 0
        set(value: Int){
            field = value
            invalidate()
        }

    private var backColor: Int = Color.parseColor("#2f302c")
        set(value: Int){
            field = value
            invalidate()
        }

    private var futureDayColor: Int = Color.argb(255,255,255,255)
        set(value: Int){
            field = value
            invalidate()
        }

    private var todayColor: Int = Color.argb(255,200,200,255)
        set(value: Int){
            field = value
            invalidate()
        }

    private var pastDayColor: Int = Color.argb(70,60,60,80)
        set(value: Int){
            field = value
            invalidate()
        }

    private var todayReference: Int = 1 // enum
        set(value: Int){
            field = value
            invalidate()
        }

    private var textTitleColor: Int = Color.parseColor("#ffffff")
        set(value: Int){
            field = value
            invalidate()
        }

    private var textDayColor: Int = Color.argb(255,0,0,0)
        set(value: Int){
            field = value
            invalidate()
        }

    private var textTaskColor: Int = Color.argb(255,205,127,63)
        set(value: Int){
            field = value
            invalidate()
        }

    private var taskIcon: String = SpritesInfo.AnimatedSprites.avatar_1.key
        set(value: String){
            field = value
            invalidate()
        }

    private var taskCount: Int = 0
        set(value: Int){
            field = value
            invalidate()
        }




    init {
        isClickable = true

        context.withStyledAttributes(attrs, R.styleable.WeekDayView) {
            try {
                getString(R.styleable.WeekDayView_dayName).let {_dayName ->
                        dayName = _dayName!!
                }
                dayNumber = getInt(R.styleable.WeekDayView_dayNumber, dayNumber)
                backColor = getColor(R.styleable.WeekDayView_backColor, backColor)
                futureDayColor = getColor(R.styleable.WeekDayView_futureDayColor, futureDayColor)
                todayColor = getColor(R.styleable.WeekDayView_todayColor, todayColor)
                pastDayColor = getColor(R.styleable.WeekDayView_pastDayColor, pastDayColor)
                todayReference = getInt(R.styleable.WeekDayView_todayReference, todayReference)
                textTitleColor = getColor(R.styleable.WeekDayView_textTitleColor, textTitleColor)
                textDayColor = getColor(R.styleable.WeekDayView_textDayColor, textDayColor)
                textTaskColor = getColor(R.styleable.WeekDayView_textTaskColor, textTaskColor)
                getString(R.styleable.WeekDayView_taskIcon).let { _taskIcon ->
                    taskIcon = _taskIcon!!
                }
                taskCount = getInt(R.styleable.WeekDayView_taskCount, taskCount)
            }finally {
                recycle()
            }
        }
    }

    var scale: Float = -1f

    var sprite: SpriteAnimation? = null
    var spritePX: Int = 0
    var spritePY: Int = 0

    var bkgColor: Int = 0


    private val paintDayNumber: Paint = Paint().apply {
        color = textDayColor
        textSize = 40f
        typeface = Typeface.DEFAULT_BOLD
    }

    private val paintTaskCount: Paint = Paint().apply {
        color = textTaskColor
        textSize = 40f
        typeface = Typeface.DEFAULT_BOLD
    }

    private val paintTextTitle: Paint = Paint().apply {
        color = textTitleColor
        textSize = 55f
        typeface = Typeface.DEFAULT_BOLD
    }
    private val paintBkgTitle: Paint = Paint().apply {
        color = backColor
        style = Paint.Style.FILL
    }

    init{
        updateValues()
    }

    fun updateValues(){
        if (todayReference == TodayReference.today.value){
            bkgColor = todayColor
            sprite = SpriteAnimation(0,0, SpritesInfo.AnimatedSprites.avatar_1)

        }else{
            bkgColor = if(todayReference == TodayReference.future.value) futureDayColor else pastDayColor
            sprite = if(taskIcon != null) SpriteAnimation(0,0,SpritesInfo.AnimatedSprites.valueOf(taskIcon)) else null
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        var w = min(w, h)
        if (todayReference == TodayReference.today.value){
            spritePX = w / 2
            spritePY = h / 2
        }else{
            w /=  2
            spritePX = w / 4
            spritePY = h * 3 / 4
        }
        scale = w / 50f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.apply {
            drawColor(bkgColor)
            sprite.let {
                sprite!!.pintarFijo(this, spritePX, spritePY, scale)
            }
        }
    }



}

enum class TodayReference(val value: Int){
    past(-1),
    today(0),
    future(1)
}