package com.pdg.tempori.util.sprite_animation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class SpriteObject implements Comparable<SpriteObject> {

    protected float posX;
    protected float posY;
    protected float tamX;
    protected float tamY;
    protected SpritesInfo.ISpriteEnum identificador;
    protected Bitmap imagen;

    protected float velX, velY;

    public SpriteObject(float posX, float posY, SpritesInfo.ISpriteEnum type) {
        super();
        this.identificador = type;
        this.imagen = SpritesInfo.Companion.getInstance().getStaticBitmap(type);
        this.tamX = imagen.getWidth();
        this.tamY = imagen.getHeight();
        this.posX = posX;
        this.posY = posY;
        this.velX = 0;
        this.velY = 0;

    }

    public void setVel(float f, float vy) {
        this.velX = f;
        this.velY = vy;
    }

    /**
     * este m�todo se encarga de actualizar la posici�n del objeto seg�n su
     * velocidad.
     *
     * @param w                 ancho del lienzo
     * @param h                 alto del lienzo
     * @param invertirDireccion true -> la posici�n que llega al l�mite multiplica su
     *                          velocidad por -1.<br>
     *                          false -> la posici�n que llega al l�mite sale al otro extremo.
     */
    public void mover(int w, int h, boolean invertirDireccion) {

        posX += velX;
        posY += velY;
        if (posX > w) {
            if (invertirDireccion) {
                velX *= -1;
            } else {
                posX = 0;
            }
        } else if (posX < 0) {
            if (invertirDireccion) {
                velX *= -1;
            } else {
                posX = w;
            }
        }
        if (posY > h) {
            if (invertirDireccion) {
                velY *= -1;
            } else {
                posY = 0;
            }
        } else if (posY < 0) {
            if (invertirDireccion) {
                velY *= -1;
            } else {
                posY = h;
            }
        }
    }

    public void pintar(Canvas c, int xCentro, int yCentro, float escala) {
        // c.drawBitmap(imagen, xCentro + posX, yCentro + posY, null);
        int cx = (int) (xCentro + posX);
        int cy = (int) (yCentro + posY);
        int tx = (int) (escala * tamX / 2);
        int ty = (int) (escala * tamY / 2);
        Rect dst = new Rect(cx - tx, cy - ty, cx + tx, cy + ty);
        c.drawBitmap(imagen, null, dst, null);
    }

    /**
     * @return the posX
     */
    public float getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(float posX) {
        this.posX = posX;
    }

    /**
     * @return the posy
     */
    public float getPosy() {
        return posY;
    }

    /**
     * @param posy the posy to set
     */
    public void setPosy(float posy) {
        this.posY = posy;
    }

    /**
     * @return the posY
     */
    public float getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(float posY) {
        this.posY = posY;
    }

    /**
     * @return the tamX
     */
    public float getTamX() {
        return tamX;
    }

    /**
     * @param tamX the tamX to set
     */
    public void setTamX(float tamX) {
        this.tamX = tamX;
    }

    /**
     * @return the tamY
     */
    public float getTamY() {
        return tamY;
    }

    /**
     * @param tamY the tamY to set
     */
    public void setTamY(float tamY) {
        this.tamY = tamY;
    }

    /**
     * @return the imagen
     */
    public Bitmap getImagen() {
        return imagen;
    }

    /**
     * @param imagen the imagen to set
     */
    public void setImagen(Bitmap imagen) {
        this.imagen = imagen;
    }

    /**
     * @return the identificador
     */
    public SpritesInfo.ISpriteEnum getIdentificador() {
        return identificador;
    }

    /**
     * @param identificador the identificador to set
     */
    public void setIdentificador(SpritesInfo.ISpriteEnum identificador) {
        this.identificador = identificador;
        setImagen(SpritesInfo.Companion.getInstance().getStaticBitmap(identificador));
    }

    @Override
    public int compareTo(SpriteObject another) {
        return (int) -(posY - another.getPosY());
    }

}
