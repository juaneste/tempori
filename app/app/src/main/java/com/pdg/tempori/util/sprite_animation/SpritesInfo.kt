package com.pdg.tempori.util.sprite_animation

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.pdg.tempori.R

class SpritesInfo private constructor() {

    private val _staticSprites: MutableMap<String, Bitmap> = mutableMapOf()
    val staticSprites: Map<String, Bitmap>
        get() = _staticSprites

    private val _animatedSprites: MutableMap<String, Bitmap> = mutableMapOf()
    val animatedSprites: Map<String, Bitmap>
        get() = _animatedSprites

    init {
        loadSprites()
    }

    fun loadSprites() {
        StaticSprites.values().forEach { stp ->
            _staticSprites.put(
                stp.key,
                BitmapFactory.decodeResource(Resources.getSystem(), stp.ref)
            )
        }
        AnimatedSprites.values().forEach { ats ->
            _animatedSprites.put(
                ats.key,
                BitmapFactory.decodeResource(Resources.getSystem(), ats.ref)
            )
        }
    }

    fun getBitmap(identifier: ISpriteEnum): Bitmap{
        return when(identifier){
            is StaticSprites -> getBitmap(identifier)
            else -> staticSprites[StaticSprites.arbol_a.name]!!
        }
    }

    fun getStaticBitmap(identifier: String): Bitmap {
        val bitmap = staticSprites[identifier]
        if (bitmap != null)
            return bitmap
        return staticSprites[StaticSprites.arbol_a.key]!!
    }

    fun getStaticBitmap(identifier: ISpriteEnum): Bitmap {
        return getStaticBitmap(identifier.key)
    }

    fun getAnimatedBitmap(identifier: String): Bitmap {
        val bitmap = animatedSprites[identifier]
        if (bitmap != null)
            return bitmap
        return animatedSprites[AnimatedSprites.materia5_0.key]!!
    }

    fun getAnimatedBitmap(identifier: ISpriteEnum): Bitmap {
        return getStaticBitmap(identifier.key)
    }


    companion object {
        @Volatile
        private var INSTANCE: SpritesInfo? = null

        fun getInstance(): SpritesInfo {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = SpritesInfo()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }

    interface ISpriteEnum{
        val key: String
        val ref: Int
        val level: Int
        val frames: Int
    }

    enum class StaticSprites(override val key: String, override val ref: Int, override val level: Int, override val frames: Int = 1) : ISpriteEnum{
        arbol_a("arbol_a", R.drawable.obj_arbol_a, 2),
        arbol_b("arbol_b", R.drawable.obj_arbol_b, 1),
        arbol_c("arbol_c", R.drawable.obj_arbol_c, 3),
        arbol_d("arbol_d", R.drawable.obj_arbol_d, 3),
        arbol_e("arbol_e", R.drawable.obj_arbol_e, 5),
        arbol_f("arbol_f", R.drawable.obj_arbol_f, 4),
        arbol_g("arbol_g", R.drawable.obj_arbol_g, 4),

        charco_a("charco_a", R.drawable.obj_charco_a, 5),
        charco_b("charco_b", R.drawable.obj_charco_b, 3),
        charco_c("charco_c", R.drawable.obj_charco_c, 2),
        charco_d("charco_d", R.drawable.obj_charco_d, 4),

        monu_a("monu_a", R.drawable.obj_monu_a, 4),
        monu_b("monu_b", R.drawable.obj_monu_b, 5),
        monu_c("monu_c", R.drawable.obj_monu_c, 3),
        monu_d("monu_d", R.drawable.obj_monu_d, 2),
        monu_e("monu_e", R.drawable.obj_monu_e, 1),

        planta_a("planta_a", R.drawable.obj_planta_a, 4),
        planta_b("planta_b", R.drawable.obj_planta_b, 4),
        planta_c("planta_c", R.drawable.obj_planta_c, 1),
        planta_d("planta_d", R.drawable.obj_planta_d, 2),
        planta_e("planta_e", R.drawable.obj_planta_e, 5),
        planta_f("planta_f", R.drawable.obj_planta_f, 5),
        planta_g("planta_g", R.drawable.obj_planta_g, 4),
        planta_h("planta_h", R.drawable.obj_planta_h, 3),
        planta_i("planta_i", R.drawable.obj_planta_i, 4),
        planta_j("planta_j", R.drawable.obj_planta_j, 2),
        planta_k("planta_k", R.drawable.obj_planta_k, 1),
        planta_l("planta_l", R.drawable.obj_planta_l, 2),
        planta_m("planta_m", R.drawable.obj_planta_m, 1),

        roca_a("roca_a", R.drawable.obj_roca_a, 2),
        roca_b("roca_b", R.drawable.obj_roca_b, 3),
        roca_c("roca_c", R.drawable.obj_roca_c, 3),
        roca_d("roca_d", R.drawable.obj_roca_d, 5),
        roca_e("roca_e", R.drawable.obj_roca_e, 5),
        roca_f("roca_f", R.drawable.obj_roca_f, 3),
        roca_g("roca_g", R.drawable.obj_roca_g, 2),
        roca_h("roca_h", R.drawable.obj_roca_h, 1),
        roca_i("roca_i", R.drawable.obj_roca_i, 4),
        roca_j("roca_j", R.drawable.obj_roca_j, 4)
    }

    enum class AnimatedSprites(override val key: String, override val ref: Int, override val frames: Int, override val level: Int = 1): ISpriteEnum {
        avatar_1("avatar_1", R.drawable.reel_temporio_1a, 5),
        avatar_1_pies("avatar_1_pies", R.drawable.reel_temporio_1b, 5),
        avatar_2("avatar_2", R.drawable.reel_temporio_2a, 5),
        avatar_2_pies("avatar_2_pies", R.drawable.reel_temporio_2b, 5),

        materia1_0("materia1_0", R.drawable.reel_perso_0, 5),
        materia1_1("materia1_1", R.drawable.reel_perso_1, 5),
        materia1_2("materia1_2", R.drawable.reel_perso_2, 5),
        materia1_3("materia1_3", R.drawable.reel_perso_3, 5),
        materia1_4("materia1_4", R.drawable.reel_perso_4, 5),
        materia1_5("materia1_5", R.drawable.reel_perso_5, 5),

        materia2_0("materia2_0", R.drawable.reel_alge_0, 5),
        materia2_1("materia2_1", R.drawable.reel_alge_1, 5),
        materia2_2("materia2_2", R.drawable.reel_alge_2, 5),
        materia2_3("materia2_3", R.drawable.reel_alge_3, 5),
        materia2_4("materia2_4", R.drawable.reel_alge_4, 5),
        materia2_5("materia2_5", R.drawable.reel_alge_5, 5),

        materia3_0("materia3_0", R.drawable.reel_logi_0, 5),
        materia3_1("materia3_1", R.drawable.reel_logi_1, 5),
        materia3_2("materia3_2", R.drawable.reel_logi_2, 5),
        materia3_3("materia3_3", R.drawable.reel_logi_3, 5),
        materia3_4("materia3_4", R.drawable.reel_logi_4, 5),
        materia3_5("materia3_5", R.drawable.reel_logi_5, 5),

        materia4_0("materia4_0", R.drawable.reel_coe_0, 5),
        materia4_1("materia4_1", R.drawable.reel_coe_1, 5),
        materia4_2("materia4_2", R.drawable.reel_coe_2, 5),
        materia4_3("materia4_3", R.drawable.reel_coe_3, 5),
        materia4_4("materia4_4", R.drawable.reel_coe_4, 5),
        materia4_5("materia4_5", R.drawable.reel_coe_5, 5),

        materia5_0("materia5_0", R.drawable.reel_intro_0, 5),
        materia5_1("materia5_1", R.drawable.reel_intro_1, 5),
        materia5_2("materia5_2", R.drawable.reel_intro_2, 5),
        materia5_3("materia5_3", R.drawable.reel_intro_3, 5),
        materia5_4("materia5_4", R.drawable.reel_intro_4, 5),
        materia5_5("materia5_5", R.drawable.reel_intro_5, 5),

        materia6_0("materia6_0", R.drawable.reel_ing_0, 5),
        materia6_1("materia6_1", R.drawable.reel_ing_1, 5),
        materia6_2("materia6_2", R.drawable.reel_ing_2, 5),
        materia6_3("materia6_3", R.drawable.reel_ing_3, 5),
        materia6_4("materia6_4", R.drawable.reel_ing_4, 5),
        materia6_5("materia6_5", R.drawable.reel_ing_5, 5),

        semestre_0("semestre_0", R.drawable.reel_planeta_0, 5),
        semestre_1("semestre_1", R.drawable.reel_planeta_1, 5),
        semestre_2("semestre_2", R.drawable.reel_planeta_2, 5),

    }
}